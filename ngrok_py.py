# Install and run ngrok in the background on a monorail instance

import json
import os
import urllib2
import zipfile
import time

# Get the application in use
atlas_software = int(input("""Select the number corresponding to your application\n
JIRA - 1 | Confluence - 2 | Bitbucket - 3 | Bamboo - 4\n\n: """))

# Decide port number
def port_choice():
    global port
    if atlas_software == 1:
        port = 8080
    elif atlas_software == 2:
        port = 8090
    elif atlas_software == 3:
        port = 7990
    elif atlas_software == 4:
        port = 8085
    return port

# Download ngrok
def dl_ngrok():
    print "Downloading File........\n"
    file_content = os.system('wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip')
    return unzip_file()


# unzip ngrok
def unzip_file():
    os.system("unzip ngrok-stable-linux-amd64.zip")
    os.remove("ngrok-stable-linux-amd64.zip")
    return set_config(port)

# Set up the config file
def set_config(port):
    with open("ngrok.yml", 'w') as f:
        f.write("authtoken: CDSsRHHj1yUDubtn7zMc_7mFGgfHR9JEUJgCMkknXR\ntunnels:\n  conf:\n    proto: http\n    addr: %d\n  ngrok:\n    proto: http\n    addr: 4040\n    inspect: false" % port)
    return run_ngrok()

# Run Ngrok
def run_ngrok():
    pd = os.getcwd()
    os.system("./ngrok start -log=stdout -config %s/ngrok.yml --all > /dev/null &" % pd)
    time.sleep(10)
    return ngrok_url()

# Show the rangom URL
def ngrok_url():
    os.system("curl  http://localhost:4040/api/tunnels > tunnels.json")

    with open('tunnels.json') as data_file:
        datajson = json.load(data_file)

    url_list = []
    for i in datajson['tunnels']:
        if i['config']['addr'] != 'localhost:4040':
            url_object = [i['public_url']]
            url_list.append(url_object)

    final_url = []
    for i in url_list:
        if i not in final_url:
            final_url.append(i)

    step_url = final_url[0]
    url = step_url[0]

    msg = ("Your ngrok url is: %s" % url)

    print (msg)



port_choice()
dl_ngrok()
