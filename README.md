Ngrok allows you to grant your Atlassian applicaiton access to the internet.

This simple Python script installs ngrok as a background process and then depending on the applicaiton you choose opens up the desired port. 

It then provides you with the randomly generated URL for which you can access it on. You can use HTTPS or HTTP.

To use this simply clone the repo and then run the script. 
